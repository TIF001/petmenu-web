/**
 *
 * main.js - retrieve the data from the json and display the menu
 *
 * @licstart  The following is the entire license notice for the
 *  JavaScript code in this page.
 *
 * Copyright (C) 2020  Andrew Jenkins
 *
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU GPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.
 *
 * @licend  The above is the entire license notice
 * for the JavaScript code in this page.
 *
 */


// double-digit
function dd(i) {
    if (i.length == 1) i = "0" + i;
    return i;
}

function dateToStr(d) {
    const tMon = dd((d.getMonth()+1).toString());
    const tDay = dd(d.getDate().toString());
    return (d.getFullYear() + "-" + tMon + "-" + tDay);
}

function dateToLabel(d){
    days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
              "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    return d.getDate()
           + " " + months[d.getMonth()];
}

function dateToLongLabel(d){
    days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday",
            "Friday", "Saturday"];
    months = ["January", "February", "March", "April", "May", "June",
              "July", "August", "September", "October", "November",
              "December"];
    return days[d.getDay()] + " " + d.getDate()
           + " " + months[d.getMonth()];
}

function nearestDateTo(datestr1, datestrs) {
    let currentNearest = "";
    let currentMinDelta = null;

    // can't be a forEach because it doesn't let you break out of it.
    for (let i=0; i< datestrs.length; i++) {
        let datestr = datestrs[i];
        // if it's the same then immediately go
        if (datestr == datestr1) return datestr1;
        delta = Math.abs(strToDate(datestr1).getTime() - strToDate(datestr).getTime())
        if (!currentMinDelta || delta < currentMinDelta ) {
            currentNearest = datestr;
            currentMinDelta = delta;
        }
    }
    return currentNearest;
}

function parseDateStr(s) {
    return s.split("-").map(a => Math.round(a));
}

function strToDate(d) {
    const ymd = parseDateStr(d)
    return new Date(ymd[0], ymd[1] - 1, ymd[2]);
}

function isBreakfastDay(d) {
    let dow = d.getDay();
    // Sun is 0, Sat is 6
    return (dow>=1 && dow<=5);
}

function dispAll(selector, show) {
    let elems = document.getElementsByClassName(selector);
    for (i=0; i < elems.length; i++) {
        if (show == "toggle") {
            elems[i].classList.toggle("hide");
        } else if (show) {
            elems[i].classList.remove("hide");
        } else {
            elems[i].classList.add("hide");
        }
    };
}

function setupEventListeners() {
    ["breakfast","lunch","dinner","brunch"].forEach(meal => {
        document.getElementById("show" + meal).addEventListener("click", e => showMeal(meal));
    });
    document.getElementById("showcalendar").addEventListener("click", e=> {blurIfClick(e); toggleCalendar()});
    document.getElementById("formal-toggle").addEventListener("click", toggleFormal);
}

let currentMeal = "";

function showMeal(mealname) {
	/* not checking if the meal is already selected here because there's a
	 * good change that this will be called again once it's been initially
	 * displayed, but with the addition of formal data. */

        dispAll("menu", false);
        if (currentMeal){
            unselectID("show" + currentMeal);
        }
        unhideID(mealname + "menu");
    	if (mealname == "dinner" && formalToday) {
		unhideID("formal-switch-container");
		console.log("showing formal toggle", formalToday)
		if (formalOn) setFormal(true);
	} else {
		console.log("hiding formal toggle", formalToday)
		hideID("formal-switch-container");
    	}

        currentMeal = mealname;
    selectID("show" + mealname);
}

var formalOn = false;
function setFormal(val) {
	if (val) {
		unhideID("formalmenu");
		hideID("dinnermenu");
		formalOn = true;
		document.getElementById("formal-toggle").textContent = "Regular servery menu";
	} else {
		hideID("formalmenu");
		unhideID("dinnermenu");
		formalOn = false;
		document.getElementById("formal-toggle").textContent = "Go formal";
	}
}
function toggleFormal() {
	setFormal(!formalOn);
}

function targetForItem(mealname, itemname) {
    return "#" + mealname +"menu ." + itemname + " ul";
}

function targetForFormalItem(itemname) {
    return "#formalmenu ." + itemname + " div";
}

// add abbreviations with mouseover
function addDietaryAbbrs(elem, abbrdict, abbrs) {
     for (i in abbrs) {
         const newAbbr = document.createElement("abbr");
         newAbbr.textContent = abbrs[i];
         newAbbr.title = abbrdict[abbrs[i]];
	 newAbbr.addEventListener("click", doExpandAbbr);
         elem.appendChild(document.createTextNode(" "));
         elem.appendChild(newAbbr);
     }
}

function doExpandAbbr(e) {
	var longform=e.target.getAttribute("title").trim();
	var current=e.target.textContent.trim();
	if (current != longform) {
		e.target.setAttribute("__petmenu_abbr", current);
		e.target.textContent = longform;
	} else {
		e.target.textContent = e.target.getAttribute("__petmenu_abbr");
	}
}

function createMealList(list, target, legend) {
    // target should be e.g. "#lunchmenu .mains ul"
    let elem = document.querySelector(target);
    // get rid of all the li's that are there.
    while (elem.firstChild) elem.removeChild(elem.firstChild);

    list.forEach(item => {
        const newLi = document.createElement("li");
        newLi.appendChild(document.createTextNode(item[0]));
        elem.appendChild(newLi);
        addDietaryAbbrs(newLi, legend, item[1]);
    });
}

function createFormalMealList(list, target, legend) {
    // target should be e.g. "#lunchmenu .mains ul"
    let elem = document.querySelector(target);
    // get rid of all the li's that are there.
    while (elem.firstChild) elem.removeChild(elem.firstChild);

    list.forEach(item => {
        const newLi = document.createElement("p");
        newLi.appendChild(document.createTextNode(item[0][0]));
        elem.appendChild(newLi);
        addDietaryAbbrs(newLi, legend, item[0][1]);
	if (item[1] !== null) {
	    const altsep = document.createElement("p");
	    altsep.appendChild(document.createTextNode("or"));
	    altsep.classList.add("or");
	    elem.appendChild(altsep);
            const newLi2 = document.createElement("p");
            newLi2.appendChild(document.createTextNode(item[1][0]));
            elem.appendChild(newLi2);
            addDietaryAbbrs(newLi2, legend, item[1][1]);
	    newLi.classList.add("option1");
	    newLi2.classList.add("option2");
	}
    });
}

function isHidden(i) {
    return document.getElementById(i).classList.contains("hide");
}

function hideID(i) {
    document.getElementById(i).classList.add("hide");
}

function unhideID(i) {
    document.getElementById(i).classList.remove("hide");
}

function disableID(i) {
    document.getElementById(i).setAttribute("disabled","true");
}

function undisableID(i) {
    document.getElementById(i).removeAttribute("disabled");
}

function selectID(i) {
    document.getElementById(i).classList.add("selected");
}

function unselectID(i) {
    document.getElementById(i).classList.remove("selected");
}

var formalToday = false;
function createMealLists(day) {
    let menutab = serverydata.menus_by_date[day];
    let data=serverydata;
	console.log("createMealLists", day);

    if (!menutab) throw "Menu for day could not be loaded"

    let datesubs = document.getElementsByClassName("mealdate");
    for (i=0; i < datesubs.length; i++) {
        datesubs[i].innerHTML = dateToLongLabel(strToDate(day));
    }

    disableID("showlunch");
    disableID("showdinner");
    disableID("showbreakfast");
    unselectID("showlunch");
    unselectID("showbreakfast");
    unselectID("showdinner");
    unselectID("showbrunch");
    // only one of breakfast or brunch will be shown - show breakfast until
    // proven brunch-y.
    hideID("showbrunch");
    unhideID("showbreakfast");
    unhideID("showlunch");

    if (menutab.hasOwnProperty("dinner")) { 
    	createMealList(menutab.dinner.mains, targetForItem("dinner", "mains"), data.di_legend);
    	createMealList(menutab.dinner.sides, targetForItem("dinner", "sides"), data.di_legend);
    	createMealList(menutab.desserts, targetForItem("dinner", "desserts"), data.di_legend);
    	/* RIP salad, Michaelmas 21 */
    	/*createMealList(menutab.salads, targetForItem("dinner", "salads"), data.di_legend);*/
	undisableID("showdinner");
    } 

    if (menutab.hasOwnProperty("lunch")) {
        createMealList(menutab.lunch.mains, targetForItem("lunch", "mains"), data.di_legend);
        createMealList(menutab.lunch.sides, targetForItem("lunch", "sides"), data.di_legend);
        createMealList(menutab.desserts, targetForItem("lunch", "desserts"), data.di_legend);
        /*createMealList(menutab.salads, targetForItem("lunch", "salads"), data.di_legend);*/
        createMealList([menutab.soup], targetForItem("lunch", "soup"), data.di_legend);
	    /* new baked potato entry for 2021 */
	if (menutab.hasOwnProperty("potato")) {
    		createMealList(menutab.potato, targetForItem("lunch", "potato"), data.di_legend);
	}
        undisableID("showlunch");
    } else if (menutab.hasOwnProperty("brunch")) {
        createMealList(menutab.brunch.regular, targetForItem("brunch", "mains"), data.di_legend);
        if (menutab.brunch.hasOwnProperty("specials")) {
            createMealList(menutab.brunch.specials,
                           targetForItem("brunch", "special"), data.di_legend);
            document.querySelector("#brunchmenu .special").classList.remove("hide");
        } else {
            document.querySelector("#brunchmenu .special").classList.add("hide");
        }
            unhideID("showbrunch");
            undisableID("showbrunch");
            hideID("showbreakfast");
            hideID("showlunch");
    }

    if (isBreakfastDay(strToDate(day))) {
        createMealList(data.breakfast, targetForItem("breakfast", "mains"), data.di_legend);
        undisableID("showbreakfast");
    }
}

function createFormalLists(day) { 

	console.log("createFormalLists", day);
    menutab = formaldata.menus_by_date[day];
    data=formaldata;
    formalToday = !(!menutab);
    if (!formalToday){
         console.log("no formal", day);
	 if (!isHidden("formalmenu")) {
	    toggleFormal();
            showMeal("dinner");
	 }
	 return
    }
    data.di_legend = [];
    const title = document.getElementById("formalname");
    title.firstChild.remove();
    title.appendChild(document.createTextNode(menutab.title));
    createFormalMealList(menutab.starter, targetForFormalItem("starter"), serverydata.di_legend);
    createFormalMealList(menutab.main, targetForFormalItem("main"), serverydata.di_legend);
    createFormalMealList(menutab.dessert, targetForFormalItem("dessert"), serverydata.di_legend);
}

/*
 * Calendar functions
 */

let selecteddate = "";

// Going back strange multiples of time to account for BST.

function prevDay() {
    let curDate = strToDate(selecteddate);
    let newDate = new Date(curDate.getTime() - 3600 * 12 * 1000);
    onDateSelect(dateToStr(newDate));
}

function nextDay() {
    let curDate = strToDate(selecteddate);
    let newDate = new Date(curDate.getTime() + 3600 * 36 * 1000);
    onDateSelect(dateToStr(newDate));
}

function blurIfClick(e) {
    if (e.detail) {
        e.target.blur();
    }
}

function nextPrevAttachListeners() {
    document.getElementById("back-day")
            .addEventListener("click", e => {blurIfClick(e); prevDay()});
    document.getElementById("forward-day")
            .addEventListener("click", e => {blurIfClick(e); nextDay()});
}

let calendarShown = false;

function toggleCalendar() {
    if (calendarShown) {
        hideCalendar();
    } else {
        showCalendar();
    }
}

function showCalendar() {
    unhideID("calendar");
    //selectID("showcalendar");
    //document.getElementById("showcalendar").classList.add("showingcal")
    calendarShown = true;
}

function hideCalendar() {
    hideID("calendar");
    //document.getElementById("showcalendar").classList.remove("showingcal")
    //unselectID("showcalendar");
    calendarShown = false;
}

function onDateSelect(datestr) {
    // re-enable all the enabled calendar dates

    es = document.getElementsByClassName("calday");
    sdate = strToDate(datestr);
    sdnext = dateToStr(new Date(sdate.getTime() + 3600 * 36 * 1000));
    sdprev = dateToStr(new Date(sdate.getTime() - 3600 * 12 * 1000));

    if (!serverydata.menus_by_date[sdnext]){
        disableID("forward-day");
    } else {
        undisableID("forward-day");
    }

    if (!serverydata.menus_by_date[sdprev]){
        disableID("back-day");
    } else {
        undisableID("back-day");
    }

    for (i=0;i<es.length;i++) {
        elem = es[i];
        if (!elem.classList.contains("calunavailable")) {
            elem.removeAttribute("disabled");
        }
        if (elem.getAttribute("data-caldate") == datestr) {
            elem.setAttribute("disabled", true);
        }
    }

    // then show the menu
    createMealLists(datestr);
    selecteddate = datestr;
    /* and now for the formal side of things */
    if (formaldata){
	createFormalLists(datestr);
    }

    /* Show the meal that was previously selected, if it exists. Otherwise,
     * show dinnner if that exists, and lunch if there is no dinner, etc.
     * If no meals (as far as we can work out), hide the whole thing.
     */
    let menutab = serverydata.menus_by_date[datestr];
	/*TODO: extend this logic so it works for formals */
    /*let formaltab = serverydata.menus_by_date[day];*/
    if (menutab[currentMeal]) showMeal(currentMeal);
    else if (menutab["dinner"]) showMeal("dinner");
    else if (menutab["lunch"]) showMeal("lunch");
    else if (menutab["brunch"]) showMeal("brunch");
    else dispAll("menu", false);
}

function caldayClick (e, data) {
    e.target.setAttribute("disabled", true);
    onDateSelect(e.target.getAttribute("data-caldate"), data);
    //hideCalendar();
}

function calendarAddHeader(cal, start_dow) {
    /*
    let thisWeek = document.createElement("div");
    thisWeek.classList.add("calheader");
    */
    let days = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
    for(let i=0; i<start_dow; i++){
        days.push(days.shift());
    }

    days.forEach(d => {
        let newElem = document.createElement("div");
        newElem.appendChild(document.createTextNode(d));
        newElem.classList.add("calheader")
        cal.appendChild(newElem);
    });
}

function calendarAddWeek(cal, weekStart) {
    //let thisWeek = document.createElement("div");
    //thisWeek.classList.add("calweek");
    let daylist = [];
    for (i = 0; i<7; i++) {
        let newDate = new Date(weekStart.getTime() + 24 * 3600 * 1000 * i);
        daylist.push(newDate);
    }
    daylist.forEach(d => {
        let thisDay = document.createElement("button");
        thisDay.appendChild(document.createTextNode(dateToLabel(d)));
        thisDay.classList.add("calday");
        if ((d.getDay() == 0) || (d.getDay() == 6)) {
            thisDay.classList.add("calweekend");
        }
        thisDay.setAttribute("data-caldate", dateToStr(d));
        cal.appendChild(thisDay);
    });
}

function highlightToday(cal) {
    let td = cal.getElementsByClassName("caltoday");
    for (i=0; i<td.length; i++) {
        td[i].classList.remove("caltoday");
    }
    todayStr = dateToStr(new Date());
    let today = document.querySelector("[data-caldate='"+todayStr+ "']");
    if (today) {
        today.classList.add("caltoday");
    }
}

function drawCalendar(elem, daylist) {
    // this is a list of the day on which weeks begin.
    let weeks = [];
    let weekstrs = [];
    // 0 for Sunday, 1 for Monday, 2 for Tuesday, etc.
    // set to 4 to go full Cantab
    let start_dow = 4;
    daylist.forEach(datestr => {
        // set to midday to cope with BST
        let date = strToDate(datestr);
        date.setHours(12);
        // this works.
        let weekStartMillis = date.getTime() - 3600 * 24 * 1000 * ((date.getDay() + 7 - start_dow) % 7);
        let newDate = new Date(weekStartMillis);
        let newStr = dateToStr(newDate)
        if (!weekstrs.includes(newStr)){
            weeks.push(newDate);
            weekstrs.push(newStr);
        }
    });
    /* first create the calendar with all weeks available */
    let cal = document.createElement("div");
    cal.classList.add("calcontainer");
    elem.appendChild(cal);
    calendarAddHeader(cal, start_dow);
    weeks.sort((d1, d2)=> {
        let d1i = d1.toISOString()
        let d2i = d2.toISOString()
        if (d2i < d1i) {
            return 1;
        } else if (d2i > d1i){
            return -1;
        }
        else{
            return 0;
        }
    });
    console.log(weeks[0])
    console.log(weeks[1]);
    weeks.forEach(w => calendarAddWeek(cal, w));
    document.querySelectorAll("[data-caldate]").forEach(e => {
        /* Now add an indicator of unavailability to all those days
           for which no menu exists */
        if (!daylist.includes(e.getAttribute("data-caldate"))) {
            e.classList.add("calunavailable");
            e.setAttribute("disabled", true);
        }
    });
    highlightToday(cal);
}

function calendarAttachListener(cal, f) {
    es = cal.getElementsByClassName("calday");
    for (i=0;i<es.length;i++) {
        es[i].addEventListener("click", f);
    }
}

function setCalTerm(tname) {
    if (tname) {
    	let el= document.getElementById("calterm");
    	el.firstChild.remove();
    	el.appendChild(document.createTextNode(tname));
    }
}

let today = new Date(Date.now());
let tMon = dd((today.getMonth()+1).toString());
let tDay = dd((today.getDate()).toString());

/* Initialise to lunch if it's before 1pm, otherwise dinner */
currentMeal = today.getHours() < 13 ? "lunch": "dinner";

today = (today.getFullYear() + "-" + tMon + "-" + tDay);
console.log(today);

setupEventListeners();

var serverydata = null;
var formaldata = null;
fetch("servery.json")
    .then(response => response.json())
    .then(d => {let cal=document.getElementById("calendar");
                drawCalendar(cal,
                             Object.keys(d.menus_by_date));
	        serverydata = d;
                calendarAttachListener(cal, e => {caldayClick(e,d)});
                nextPrevAttachListeners(d);
	        setCalTerm(d.term_name);
                onDateSelect(nearestDateTo(today, Object.keys(d.menus_by_date)));
                unhideID("mtcontainer");
                unhideID("dateandcal");
            });

/* It is assumed that for every day there is a formal, there will also be a
 * regular hall, so don't need to check the dates here. */
fetch("formal.json")
    .then(response => response.json())
    .then(d => {
	    formaldata=d;
	    createFormalLists(nearestDateTo(today, Object.keys(serverydata.menus_by_date)));
	    /* ADJ Mar '22 fixes bug where formal didn't load on current day. */
	    if (currentMeal == "dinner" && formalToday) {
		unhideID("formal-switch-container");
		console.log("showing formal toggle after loading formaldata", formalToday)}
    });

